package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.List;

public class ListTest 
{
	private List lista;
	
	@Before
	public void sceneSetup1()
	{
		lista = new List();
	}
	
	public void sceneSetup2()
	{
		lista.add(new String("Primero"));
		lista.add(new String("Segundo"));
		lista.add(new String("Tercero"));
		lista.add(new String("Cuarto"));
	}
	
	@Test
	public void agregarTest()
	{
		assertNull("La lista no esta vacia", lista.returnFirst());
		assertNull("La lista no esta vacia", lista.returnLast());
		
		lista.add(new String("Primero"));
		lista.add(new String("Segundo"));
		
		assertTrue("No se agrego el primer elemento", lista.returnFirst().equals("Segundo"));
		assertTrue("No se agrego el segundo elemento", lista.returnLast().equals("Primero"));
		assertTrue("No se inicializo current", lista.returnFirst().equals(lista.getCurrent()));
		assertEquals("No se aumenta la variable de tamanio", lista.size(), 2);
	}
	
	@Test
	public void buscarElementoTest()
	{
		sceneSetup2();
		
		assertTrue("No se encuentra el primer elemento", lista.get(new String("Cuarto")).equals("Cuarto"));
		assertTrue("No se encuentra el ultimo elemento", lista.get(new String("Primero")).equals("Primero"));
		assertTrue("No se encuentra el elemento de la mitad", lista.get(new String("Tercero")).equals("Tercero"));
		assertNull("No se retorna el valor esperado", lista.get(new String("Quinto")));
	}
	
	@Test
	public void buscarIndiceTest()
	{
		sceneSetup2();
		
		assertTrue("No se encuentra el primer elemento", lista.get(0).equals("Cuarto"));
		assertTrue("No se encuentra el ultimo elemento", lista.get(3).equals("Primero"));
		assertTrue("No se encuentra el elemento del medio", lista.get(2).equals("Segundo"));
		assertNull("No se retorna el valor esperado", lista.get(4));
	}
	
	@Test
	public void eliminarTest()
	{
		sceneSetup2();
		
		lista.delete(new String("Cuarto"));
		
		assertTrue("No se elimino el primer elemento", lista.returnFirst().equals("Tercero"));
		
		lista.delete(new String("Primero"));
		
		assertTrue("No se elimino el ultimo elemento", lista.returnLast().equals("Segundo"));
		
		lista.add(new String("Nuevo"));
		lista.delete(new String("Tercero"));
		
		assertNull("No se borro el elemento intermedio", lista.get("Tercero"));
		assertEquals("No se redujo el tamanio de la lista", lista.size(), 2);
	}
	
	@Test
	public void listingTest()
	{
		sceneSetup2();
		
		lista.next();
		
		assertTrue("No se avanza correctamente", lista.getCurrent().equals("Tercero"));
		assertTrue("El metodo next no funciona de forma adecuada", lista.next().equals("Segundo"));
		
		lista.listing();
		
		assertTrue("La lista no regresa al inicio adecuadamente", lista.getCurrent().equals("Cuarto"));
	}
}
