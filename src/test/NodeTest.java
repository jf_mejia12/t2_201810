package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Node;

public class NodeTest<T> 
{
	private Node<String> primero;
	private Node<String> segundo;
	private Node<String> tercero;
	
	public void setupScene1()
	{
		primero = new Node<String>(new String("Primero"));
		segundo = new Node<String>(new String("Segundo"));
		tercero = new Node<String>(new String("Tercero"));
	}
	
	@Test
	public void returnElemTest()
	{
		setupScene1();
		assertTrue("No se retorna el elemento esperado", primero.returnElement().equals("Primero"));
		assertTrue("No se retorna el elemento esperado", segundo.returnElement().equals("Segundo"));
		assertTrue("No se retorna el elemento esperado", tercero.returnElement().equals("Tercero"));
	}
	
	@Test
	public void ordenTest()
	{
		setupScene1();
		primero.changeNext(segundo);
		segundo.changeNext(tercero);
		segundo.changePrevious(primero);
		tercero.changePrevious(segundo);
		
		assertNull("El primer nodo tiene un nodo previo", primero.returnPrevious());
		assertTrue("Los nodos no estan correctamente conectados", primero.returnNext().returnElement().equals("Segundo"));
		assertTrue("Los nodos no estan correctamente conectados", segundo.returnPrevious().returnElement().equals("Primero"));
		assertTrue("Los nodos no estan correctamente conectados", segundo.returnNext().returnElement().equals("Tercero"));
		assertTrue("Los nodos no estan correctamente conectados", tercero.returnPrevious().returnElement().equals("Segundo"));
		assertNull("El ultimo nodo tiene un nodo despues", tercero.returnNext());
	}
}
