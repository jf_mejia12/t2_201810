package model.data_structures;

public class Node<T> {
	
	private Node<T> next;
	
	private Node<T> previous;
	
	private T element;
	
	public Node(T object)
	{
		next = null;
		previous = null;
		element = object;
	}
	
	public T returnElement()
	{
		return element;
	}
	
	public void changeElement(T object)
	{
		element = object;
	}
	
	public void changeNext(Node<T> pNext)
	{
		next = pNext;
	}
	
	public void changePrevious(Node<T> pPrevious)
	{
		previous = pPrevious;
	}
	
	public Node<T> returnNext()
	{
		return next;
	}
	
	public Node<T> returnPrevious()
	{
		return previous;
	}

}
