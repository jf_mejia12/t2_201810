package model.data_structures;

public class List<T extends Comparable<T>> implements LinkedList<T> {
	
	private Node<T> first;
	
	private Node<T> last;
	
	private Node<T> current;
	
	private int size;
	
	public List()
	{
		first = null;
		last = null;
		current = null;
		size = 0;
	}
	
	public T returnFirst()
	{
		if(first!=null)
		{
			return (T) first.returnElement();
		}
		else return null;
	}
	
	public T returnLast()
	{
		if(first!= null)
		{
			return (T) last.returnElement();
		}
		else return null;
	}

	@Override
	public void add(T object) 
	{
		Node<T> newNode = new Node<T>(object);
		if(first == null)
		{
			first = newNode;
			last = newNode;
			current = newNode;
			size++;
		}
		else
		{
			first.changePrevious(newNode);
			newNode.changeNext(first);
			first = newNode;
			current = newNode;
			size++;
		}
	}

	@Override
	public void delete(T object) 
	{
		if(first!=null)
		{
			if(object.compareTo(first.returnElement())==0)
			{
				first = first.returnNext();
				current = first;
				if(first!=null)
				{
					first.changePrevious(null);
				}
				else
				{
					last = null;
				}
				size--;
			}

			else if(object.compareTo(last.returnElement())==0)
			{
				last = last.returnPrevious();
				last.changeNext(null);
				size--;
			}

			else
			{
				Node<T> actual = first.returnNext();
				while(actual.returnNext()!=null)
				{
					if(object.compareTo(actual.returnElement())==0)
					{
						Node<T> auxNext = actual.returnNext();
						auxNext.changePrevious(actual.returnPrevious());
						actual.returnPrevious().changeNext(auxNext);
						size--;
					}
					actual = actual.returnNext();
				}
			}
		}

	}

	@Override
	public T get(T object) 
	{
		if(first!=null)
		{
			if(object.compareTo(first.returnElement())==0)
			{
				return (T) first.returnElement();
			}

			else if(object.compareTo(last.returnElement())==0)
			{
				return (T) last.returnElement();
			}

			else
			{
				Node<T> actual = first;
				while(actual.returnNext() != null)
				{
					if(object.compareTo(actual.returnElement())==0)
					{
						return (T) actual.returnElement();
					}
					actual = actual.returnNext();
				}
			}
		}
		return null;
	}

	@Override
	public int size() 
	{
		return size;
	}

	@Override
	public T get(int position) 
	{
		if(first!=null)
		{
			if(position == 0)
			{
				return (T) first.returnElement();
			}
			else if(position == size-1)
			{
				return (T) last.returnElement();
			}
			else
			{
				int contador = 0;
				Node<T> actual = first;
				while(actual.returnNext()!=null)
				{
					if(contador == position)
					{
						return (T) actual.returnElement();
					}
					contador++;
					actual = actual.returnNext();
				}
			}
		}
		return null;

	}

	@Override
	public void listing() 
	{
		current = first;
		
	}

	@Override
	public T getCurrent() 
	{
		if(current!=null)
		{
			return (T) current.returnElement();
		}
		return null;
	}

	@Override
	public T next() 
	{
		if(current!=null)
		{
			if(current.returnElement()!=null)
			{
				current = current.returnNext();
				return (T) current.returnElement();
			}
		}
		return null;
	}

}
