package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String tripId;
	
	private String taxiId;
	
	private int tripSeconds;
	
	private double tripMiles;
	
	private double tripTotal;
	
	private int dropoffLocation;
	
	public Service(String pTripId, String pTaxiId, int pTripSeconds, double pTripMiles, double pTripTotal, int pDropoffLocation)
	{
		tripId = pTripId;
		taxiId = pTaxiId;
		tripSeconds = pTripSeconds;
		tripMiles = pTripMiles;
		tripTotal = pTripTotal;
		dropoffLocation = pDropoffLocation;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() 
	{
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() 
	{
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() 
	{
		return tripTotal;
	}
	
	public int getDropoffLocation()
	{
		return dropoffLocation;
	}

	@Override
	public int compareTo(Service o) 
	{
		return o.tripId.compareToIgnoreCase(tripId);
	}
}
