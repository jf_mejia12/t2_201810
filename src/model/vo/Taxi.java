package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxiId;
	
	private String company;
	
	public Taxi(String pTaxiId, String pCompany)
	{
		taxiId = pTaxiId;
		company = pCompany;
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() 
	{
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) 
	{
		return o.taxiId.compareToIgnoreCase(taxiId);
	}	
}
