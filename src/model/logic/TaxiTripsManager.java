package model.logic;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.List;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class TaxiTripsManager implements ITaxiTripsManager {

	private List<Taxi> listaTaxi;
	private List<Service> listaServicios;
	
	public TaxiTripsManager()
	{
		listaTaxi = new List<Taxi>();
		listaServicios = new List<Service>();
	}
	
	public void loadServices (String serviceFile) 
	{
		JsonParser parser = new JsonParser(); 
		
		try
		{
			String taxiDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";
			JsonArray arr = (JsonArray) parser.parse(new FileReader(taxiDatos));
			
			for(int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj = (JsonObject) arr.get(i);
				
				String taxi_id = "NaN";
				if ( obj.get("taxi_id") != null )
				{ taxi_id = obj.get("taxi_id").getAsString(); }
				
				String trip_id = "NaN";
				if ( obj.get("trip_id") != null )
				{ trip_id = obj.get("trip_id").getAsString(); }
				
				String company = "NaN";
				if ( obj.get("company") != null )
				{ company = obj.get("company").getAsString(); }
				
				double trip_seconds = 0;
				if ( obj.get("trip_seconds") != null )
				{ trip_seconds = obj.get("trip_seconds").getAsDouble(); }
				
				double trip_miles = 0;
				if ( obj.get("trip_miles") != null )
				{ trip_miles = obj.get("trip_miles").getAsDouble(); }
				
				double trip_total = 0;
				if ( obj.get("trip_total") != null )
				{ trip_total = obj.get("trip_total").getAsDouble(); }
				
				double dropoff_community_area = 0;
				if ( obj.get("dropoff_community_area") != null )
				{ dropoff_community_area = obj.get("dropoff_community_area").getAsDouble(); }
				
				Taxi pTaxi = new Taxi(taxi_id, company);
				Service pService = new Service(trip_id, taxi_id, (int)trip_seconds, trip_miles, trip_total, (int)dropoff_community_area);
				
				listaTaxi.add(pTaxi);
				listaServicios.add(pService);
			}
		}
		catch(JsonIOException e1)
		{
			e1.printStackTrace();
		}
		catch(JsonSyntaxException e2)
		{
			e2.printStackTrace();
		}
		catch(FileNotFoundException e3)
		{
			e3.printStackTrace();
		}
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) 
	{
		LinkedList<Taxi> resultado = new List<Taxi>();
		
		for(int i = 0; i < listaTaxi.size(); i++)
		{
			Taxi aux = (Taxi) listaTaxi.get(i);
			if(aux.getCompany().equals(company))
			{
				Taxi aux2 = resultado.get(aux);
				if(aux2 == null)
				{
					resultado.add(aux);
				}
			}
		}
		return resultado;
		
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) 
	{
		LinkedList<Service> resultado = new List<Service>();
		
		for (int i = 0; i < listaServicios.size(); i++)
		{
			Service aux = (Service) listaServicios.get(i);
			if(aux.getDropoffLocation() == communityArea)
			{
				resultado.add(aux);
			}
		}
		return resultado;
	}


}
